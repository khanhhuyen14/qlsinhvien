export const InfoStudent = [
    {
        'id': 1,
        'name': 'Nguyễn Văn A',
        'classSt': '67XD2',
        'date': '23/06/2004',
        'address': 'Hà Nam',
        'phone': '0389638274'
    },
    {
        'id': 2,
        'name': 'Nguyễn Văn B',
        'classSt': '66CS1',
        'date': '12/03/2003',
        'address': 'Bắc Ninh',
        'phone': '0358764652'
    },
    {
        'id': 3,
        'name': 'Nguyễn Văn C',
        'classSt': '67XD2',
        'date': '06/06/2004',
        'address': 'Hà Nội',
        'phone': '0389638274'
    },
    {
        'id': 4,
        'name': 'Nguyễn Văn D',
        'classSt': '66KD1',
        'date': '02/07/2003',
        'address': 'Vĩnh Phúc',
        'phone': '0389638274'
    },
    {
        'id': 5,
        'name': 'Nguyễn Văn E',
        'classSt': '65KM3',
        'date': '03/05/2002',
        'address': 'Nghệ An',
        'phone': '0389638274'
    },
    {
        'id': 6,
        'name': 'Nguyễn Văn F',
        'classSt': '67TH2',
        'date': '09/12/2004',
        'address': 'Hà Nội',
        'phone': '0389638274'
    },
    {
        'id': 7,
        'name': 'Nguyễn Văn G',
        'classSt': '67IT5',
        'date': '23/06/2004',
        'address': 'Thanh Hoá',
        'phone': '0389638274'
    },
    {
        'id': 8,
        'name': 'Nguyễn Văn H',
        'classSt': '65XD4',
        'date': '19/10/2002',
        'address': 'Đà Nẵng',
        'phone': '0389638274'
    },
    {
        'id': 9,
        'name': 'Nguyễn Văn I',
        'classSt': '67XD2',
        'date': '23/06/2004',
        'address': 'Hà Nam',
        'phone': '0389638274'
    },
    {
        'id': 10,
        'name': 'Nguyễn Văn J',
        'classSt': '67XD2',
        'date': '23/06/2004',
        'address': 'Hà Nam',
        'phone': '0389638274'
    },
    {
        'id': 11,
        'name': 'Nguyễn Văn K',
        'classSt': '67XD2',
        'date': '23/06/2004',
        'address': 'Hà Nam',
        'phone': '0389638274'
    },
    {
        'id': 12,
        'name': 'Nguyễn Văn L',
        'classSt': '67XD2',
        'date': '23/06/2004',
        'address': 'Hà Nam',
        'phone': '0389638274'
    },
    {
        'id': 13,
        'name': 'Nguyễn Văn M',
        'classSt': '67XD2',
        'date': '23/06/2004',
        'address': 'Hà Nam',
        'phone': '0389638274'
    },
    {
        'id': 14,
        'name': 'Nguyễn Văn N',
        'classSt': '67XD2',
        'date': '23/06/2004',
        'address': 'Hà Nam',
        'phone': '0389638274'
    },
    {
        'id': 15,
        'name': 'Nguyễn Văn O',
        'classSt': '67XD2',
        'date': '23/06/2004',
        'address': 'Hà Nam',
        'phone': '0389638274'
    },
    {
        'id': 16,
        'name': 'Nguyễn Văn P',
        'classSt': '67XD2',
        'date': '23/06/2004',
        'address': 'Hà Nam',
        'phone': '0389638274'
    },
    {
        'id': 17,
        'name': 'Nguyễn Văn Q',
        'classSt': '67XD2',
        'date': '23/06/2004',
        'address': 'Hà Nam',
        'phone': '0389638274'
    },
    {
        'id': 18,
        'name': 'Nguyễn Văn R',
        'classSt': '67XD2',
        'date': '23/06/2004',
        'address': 'Hà Nam',
        'phone': '0389638274'
    },
    {
        'id': 19,
        'name': 'Nguyễn Văn S',
        'classSt': '67XD2',
        'date': '23/06/2004',
        'address': 'Hà Nam',
        'phone': '0389638274'
    },
    {
        'id': 20,
        'name': 'Nguyễn Văn T',
        'classSt': '67XD2',
        'date': '23/06/2004',
        'address': 'Hà Nam',
        'phone': '0389638274'
    },
    {
        'id': 21,
        'name': 'Nguyễn Văn U',
        'classSt': '67XD2',
        'date': '23/06/2004',
        'address': 'Hà Nam',
        'phone': '0389638274'
    },
    {
        'id': 22,
        'name': 'Nguyễn Văn V',
        'classSt': '67XD2',
        'date': '23/06/2004',
        'address': 'Hà Nam',
        'phone': '0389638274'
    },
    {
        'id': 23,
        'name': 'Nguyễn Văn W',
        'classSt': '67XD2',
        'date': '23/06/2004',
        'address': 'Hà Nam',
        'phone': '0389638274'
    },
    {
        'id': 24,
        'name': 'Nguyễn Văn X',
        'classSt': '67XD2',
        'date': '23/06/2004',
        'address': 'Hà Nam',
        'phone': '0389638274'
    },
    {
        'id': 25,
        'name': 'Nguyễn Văn Y',
        'classSt': '67XD2',
        'date': '23/06/2004',
        'address': 'Hà Nam',
        'phone': '0389638274'
    },
    {
        'id': 26,
        'name': 'Nguyễn Văn Z',
        'classSt': '67XD2',
        'date': '23/06/2004',
        'address': 'Hà Nam',
        'phone': '0389638274'
    }
]