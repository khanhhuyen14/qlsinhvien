import React from 'react'

const Table = (props) => {

  return (

    <div className='table'>
      <table className="table">
        <thead>
          <tr>
            <th scope="col">STT</th>
            <th scope="col">MSSV</th>
            <th scope="col">Họ tên</th>
            <th scope="col">Lớp</th>
            <th scope="col">Ngày sinh</th>
            <th scope="col">Quê quán</th>
            <th scope="col">Số điện thoại</th>
          </tr>
        </thead>
            <tbody>
              <tr>
                <th scope="row">{props.stt}</th>
                <td>{props.mssv}</td>
                <td>{props.name}</td>
                <td>{props.classSt}</td>
                <td>{props.date}</td>
                <td>{props.address}</td>
                <td>{props.phone}</td>
              </tr>
            </tbody>
      </table>
    </div>
  )
}

export default Table