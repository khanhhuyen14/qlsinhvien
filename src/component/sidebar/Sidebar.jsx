import React from 'react'
import { Link } from 'react-router-dom'
import './Sidebar.css'

const Sidebar = () => {
  return (
    <div className='sidebar'>
      <div className="logo">
        <img src="https://static.vecteezy.com/system/resources/thumbnails/008/154/360/small/student-logo-vector.jpg" alt='logo'/>
      </div>
      <div className="menu">
        <p className="menu-item"><Link className='link' to='/'>Trang chủ</Link></p>
        <p className="menu-item"><Link className='link' to='search'>Tìm kiếm sinh viên</Link></p>
        <p className="menu-item"><Link className='link' to='add'>Thêm sinh viên</Link></p>
        <p className="menu-item"><Link className='link' to='watch'>Xem danh sách</Link></p>
      </div>
    </div>
  )
}

export default Sidebar