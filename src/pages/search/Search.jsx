import React, { useState } from 'react'
import './Search.css'
import { InfoStudent } from '../../infoSt'
import Table from '../../component/table/Table'


const Search = () => {

  const [nameSt, setNameSt] = useState('')

  const [mssv, setMssv] = useState('')
  const [name, setName] = useState('')
  const [classSt, setClassSt] = useState('')
  const [date, setDate] = useState('')
  const [address, setAddress] = useState('')
  const [phone, setPhone] = useState('')

  const [isActiveDel, setActiveDel] = useState(false)
  const [isActiveUpd, setActiveUpd] = useState(false)

  const handleToggleDel = () => {
    setActiveDel(!isActiveDel);
  };

  const handleToggleUpd = () => {
    setActiveUpd(!isActiveUpd);
  };

  const handleSubmit = e => {
    e.preventDefault()
  }

  return (
    <div className='search'>


      {/* Tìm kiếm */}
      <form className='search-group col-8'>
        <label className="form-label">Tìm kiếm theo tên</label>
        <input type="text" placeholder='Nhập tên' className="form-control" name='name' Value={nameSt}
          onChange={e => setNameSt(e.target.value)} />
      </form>

      <div className="result">
        {InfoStudent.filter(student => student.name.toLowerCase() === nameSt.toLowerCase()).map(student => (
          <>
            <Table
              stt={InfoStudent.indexOf(student) + 1}
              mssv={student.id}
              name={student.name}
              classSt={student.classSt}
              date={student.date}
              address={student.address}
              phone={student.phone}
            />
            <div className="act">

              <button type="button" className="btn btn-dark"
                onClick={handleToggleUpd}
              >Sửa</button>

              <button type="button" className="btn btn-dark"
                onClick={handleToggleDel}
              >Xoá</button>


              {/* Modal xoá */}
              <div className={isActiveDel ? null : 'hidden'}>
                <div className="modalDel">
                  <div className='modal-inner'>
                    <h4>Bạn muốn xoá sinh viên này?</h4>
                    <div className="act">
                      <button type="button" className="btn btn-dark"
                        onClick={() => {
                          InfoStudent.splice(InfoStudent.indexOf(student), 1)
                          console.log(InfoStudent)
                          setActiveDel(!isActiveDel)
                          alert('Đã xoá sinh viên ' + student.name)
                        }}
                      >Xoá</button>
                      <button type="button" className="btn btn-light"
                        onClick={handleToggleDel}
                      >Trở lại</button>
                    </div>
                  </div>
                </div>
              </div>


              {/* Sửa */}
              <form onSubmit={handleSubmit} className={isActiveUpd ? null : 'hidden'}>
                <div className='form-update'>
                  <div className="form-inner">
                    <div className="mb-3">
                      <label className="form-label">MSSV</label>
                      <input type="text" className="form-control" name='mssv' defaultValue={mssv}
                        onChange={e => setMssv(e.target.value)} />
                    </div>
                    <div className="mb-3">
                      <label className="form-label">Họ tên</label>
                      <input type="text" className="form-control" name='name' defaultValue={name}
                        onChange={e => setName(e.target.value)} />
                    </div>
                    <div className="mb-3">
                      <label className="form-label">Lớp</label>
                      <input type="text" className="form-control" name='class' defaultValue={classSt}
                        onChange={e => setClassSt(e.target.value)} />
                    </div>
                    <div className="mb-3">
                      <label className="form-label">Ngày sinh</label>
                      <input type="text" className="form-control" name='date' defaultValue={date}
                        onChange={e => setDate(e.target.value)} />
                    </div>
                    <div className="mb-3">
                      <label className="form-label">Quê quán</label>
                      <input type="text" className="form-control" name='address' defaultValue={address}
                        onChange={e => setAddress(e.target.value)} />
                    </div>
                    <div className="mb-3">
                      <label className="form-label">Số điện thoại</label>
                      <input type="text" className="form-control" name='phone' defaultValue={phone}
                        onChange={e => setPhone(e.target.value)} />
                    </div>
                    <button type="submit" className="btn btn-dark"
                      onClick={() => {
                        InfoStudent[InfoStudent.indexOf(student)] = ({
                          'id': mssv,
                          'name': name,
                          'classSt': classSt,
                          'date': date,
                          'address': address,
                          'phone': phone
                        })
                        console.log(InfoStudent)
                        setActiveUpd(!isActiveUpd);
                        alert('Đã cập nhật thông tin sinh viên!')
                      }}
                    >Cập nhật</button>
                    <button type="submit" className="btn btn-dark"
                      onClick={handleToggleUpd}>Trở lại</button>
                  </div>
                </div>
              </form>
            </div>
          </>
        ))}
      </div>
    </div>
  )
}

export default Search