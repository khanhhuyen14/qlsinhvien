import React, { useState } from 'react'
import { InfoStudent } from '../../infoSt';
import './Watch.css'

const Watch = () => {

  // Cắt mảng
  function Paginator(items, page, per_page) {
    var page = page || 1,
      per_page = per_page || 10,
      offset = (page - 1) * per_page,
      paginatedStudents = items.slice(offset).slice(0, per_page)
    return paginatedStudents
  }
  const total_pages = Math.ceil(InfoStudent.length / 8);


  // Đánh số trang
  let items = [];
  let data = []
  for (let number = 1; number <= total_pages; number++) {
    items.push(number);
    data[number] = Paginator(InfoStudent, number, 8)
  }

  const [pageData, setPageData] = useState(data[1])

  return (
    <div className='watch'>
      <table className="table">
        <thead>
          <tr>
            <th scope="col">STT</th>
            <th scope="col">MSSV</th>
            <th scope="col">Họ tên</th>
            <th scope="col">Lớp</th>
            <th scope="col">Ngày sinh</th>
            <th scope="col">Quê quán</th>
            <th scope="col">Số điện thoại</th>
          </tr>
        </thead>
        {pageData.map((student, i) => (
          <tbody>
            <tr key={i}>
              <th scope="row">{InfoStudent.indexOf(student) + 1}</th>
              <td>{student.id}</td>
              <td>{student.name}</td>
              <td>{student.classSt}</td>
              <td>{student.date}</td>
              <td>{student.address}</td>
              <td>{student.phone}</td>
            </tr>
          </tbody>
        ))}
      </table>
      <div className='page-number d-flex'>
      {items.map((item, index) =>
          <button className='btn btn-dark'
            onClick={() => {
              setPageData(data[index + 1])
            }}>{item}</button>
            )}
            </div>
    </div >
  )
}

export default Watch