import React, { useState } from 'react'
import './Add.css'
import { InfoStudent } from '../../infoSt'
import Table from '../../component/table/Table'

const Add = () => {

  const [mssv, setMssv] = useState('')
  const [name, setName] = useState('')
  const [classSt, setClassSt] = useState('')
  const [date, setDate] = useState('')
  const [address, setAddress] = useState('')
  const [phone, setPhone] = useState('')

  const handleSubmit = e => {
    e.preventDefault()
    e.target.reset()
  }

  return (
    <div className='add'>
      <Table
        stt={InfoStudent.length + 1}
        mssv={mssv}
        name={name}
        classSt={classSt}
        date={date}
        address={address}
        phone={phone}
      />
      <form onSubmit={handleSubmit} className='form-add'>
        <div className="mb-3 form-item">
          <label className="form-label">MSSV</label>
          <input type="text" className="form-control" name='mssv' defaultValue={mssv}
            onChange={e => setMssv(e.target.value)} />
        </div>
        <div className="mb-3 form-item">
          <label className="form-label">Họ tên</label>
          <input type="text" className="form-control" name='name' defaultValue={name}
            onChange={e => setName(e.target.value)} />
        </div>
        <div className="mb-3">
          <label className="form-label">Lớp</label>
          <input type="text" className="form-control" name='classSt' defaultValue={classSt}
            onChange={e => setClassSt(e.target.value)} />
        </div>
        <div className="mb-3">
          <label className="form-label">Ngày sinh</label>
          <input type="text" className="form-control" name='date' defaultValue={date}
            onChange={e => setDate(e.target.value)} />
        </div>
        <div className="mb-3">
          <label className="form-label">Quê quán</label>
          <input type="text" className="form-control" name='address' defaultValue={address}
            onChange={e => setAddress(e.target.value)} />
        </div>
        <div className="mb-3">
          <label className="form-label">Số điện thoại</label>
          <input type="text" className="form-control" name='phone' defaultValue={phone}
            onChange={e => setPhone(e.target.value)} />
        </div>
        <button type="submit" className="btn btn-dark"
          onClick={() => {
            InfoStudent.push({
              'id': mssv,
              'name': name,
              'classSt': classSt,
              'date': date,
              'address': address,
              'phone': phone
            })
            console.log(InfoStudent)
            alert('Đã thêm sinh viên thành công!')
            setMssv('')
            setName('')
            setClassSt('')
            setDate('')
            setPhone('')
            setAddress('')
          }}
        >Thêm</button>
      </form>
    </div>
  )
}

export default Add