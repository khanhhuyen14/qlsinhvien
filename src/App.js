import Watch from "./pages/watch/Watch";
import Home from "./pages/home/Home";
import Sidebar from "./component/sidebar/Sidebar";
import Add from "./pages/add/Add"
import Search from "./pages/search/Search";
import './App.css'

import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

function App() {
  return (
    <div className="app d-flex">
      <Router>
        <div className="menu col-2">
          <Sidebar />
        </div>
        <div className="content col-10">
          <Routes>
            <Route path='/' element={<Home />} />
            <Route path='search' element={<Search />} />
            <Route path='add' element={<Add />} />
            <Route path='watch' element={<Watch />} />
          </Routes>
        </div>
      </Router>
    </div>
  );
}

export default App;
